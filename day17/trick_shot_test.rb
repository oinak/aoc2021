# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './trick_shot'

describe TrickShot do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { TrickShot.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) { 'target area: x=20..30, y=-10..-5' }

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(45)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(112)
    end
  end
end
