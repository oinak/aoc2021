# frozen_string_literal: true

require 'debug'

# Advent of code - Day 17
class TrickShot
  attr_reader :target

  def initialize(filename)
    data = File.read(filename)
    parse_data(data)
  end

  def solution(advanced: false)
    advanced ? target_count : highest_y
  end

  def highest_y
    record = { x: 0, y: 0, maxy: -Float::INFINITY }
    new_record = candidates.each_with_object(record) do |(xv, yv), h|
      t = shot(xv, yv)
      next h unless target?(t)

      maxy = t.map(&:last).max
      h.merge!(x: xv, y: yv, maxy:) if maxy > record[:maxy]
    end
    new_record.tap { |o| puts o.inspect }[:maxy]
  end

  ## PART 2
  def target_count
    candidates.count { |xv, yv| target?(shot(xv, yv)) }
  end
  ## end PART 2

  def candidates
    xx = (1..(2 * target[:x].max)).to_a
    yy = (target[:y].min..(2 * target[:y].max).abs).to_a
    xx.product(yy)
  end

  TARGET_RE = /target\ area:\s
    x=(?<x_min>-?\d+)\.\.(?<x_max>-?\d+),\s
    y=(?<y_min>-?\d+)\.\.(?<y_max>-?\d+)/x

  def parse_data(data)
    md = TARGET_RE.match(data)
    return unless md

    @target = {
      x: (md[:x_min].to_i..md[:x_max].to_i),
      y: (md[:y_min].to_i..md[:y_max].to_i)
    }
  end

  def shot(x_velocity, y_velocity)
    x = 0
    y = 0
    trajectory = [[x, y]]
    while x <= target[:x].max && y >= target[:y].min
      x, y, x_velocity, y_velocity = step(x, y, x_velocity, y_velocity)
      trajectory << [x, y]
    end
    trajectory
  end

  def target?(trajectory)
    trajectory.any? { |x, y| target[:x].include?(x) && target[:y].include?(y) }
  end

  def step(posx, posy, x_velocity, y_velocity)
    [posx + x_velocity,
     posy + y_velocity,
     x_velocity == 0 ? 0 : x_velocity - 1,
     y_velocity - 1]
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = TrickShot.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
