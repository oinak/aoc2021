# frozen_string_literal: true

require 'debug'

# Advent of code - Day 15
class Chiton
  attr_reader :advanced

  def initialize(filename)
    @rows = File.readlines(filename).map { |line| line.strip.chars.map(&:to_i) }
    @start = [0, 0]
  end

  def solution(advanced: false)
    @advanced = advanced
    risk_of_safest_path
  end

  def finish
    [width - 1, height - 1]
  end

  ## PART 1

  def risk_of_safest_path
    @risk = Hash.new(Float::INFINITY)
    @risk[@start] = 0

    unfold_map if advanced

    visit_nodes(coordinates, finish)

    @risk[finish] - @risk[@start]
  end

  # TODO: try UCS from https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
  def visit_nodes(sorted_unvisited, finish)
    walked_nodes = {}

    until sorted_unvisited.empty?
      # node = sorted_unvisited.min_by { |u| @risk[u] }
      # sorted_unvisited.delete(node)
      # using a priority queue has huge impact
      node = sorted_unvisited.shift

      return if node == finish

      walked_nodes[node] = true
      neighbors(*node).each do |neighbor|
        next if walked_nodes.key?(neighbor) # much faster but wrong result on large data set :-(

        tentative = @risk[node] + @rows[neighbor[1]][neighbor[0]]
        if tentative < @risk[neighbor]
          @risk[neighbor] = tentative
          binary_insert(sorted_unvisited, neighbor) unless sorted_unvisited.include? neighbor
        end
      end
    end
  end

  def visit_nodes(_, finish)
    node = @start
    frontier = [@start] # priority queue containing node only
    explored = {} # empty set
    until frontier.empty?
      node = frontier.shift
      return if node == finish

      explored[node] = true
      neighbors(*node).each do |n|
        next if explored.key? n

        if frontier.include?(n)
          if @risk[n] > @risk[node]
            frontier = frontier - [n] + [node]
            # @risk[]
          end
        else
          @risk[n] = @risk[node] + @rows[n[1]][n[0]]
          binary_insert(frontier, n)
        end
      end
    end
  end

  def binary_insert(nodes, node)
    debugger
    return nodes.insert(0, node) if nodes.empty?

    index = [*nodes.each_with_index].bsearch { |x, _| @risk[x] >= @risk[node] }.last
    nodes.insert(index, node)
  end

  def neighbors(posx, posy)
    nwes = [[posx, posy - 1], [posx - 1, posy], [posx + 1, posy], [posx, posy + 1]]
    nwes.select { |(col, row)| col >= 0 && row >= 0 && row < height && col < width }
  end

  def height= @height ||= @rows.size

  def width= @width ||= @rows[0].size

  def val(col, row)
    @rows[row][col]
  end

  def coordinates
    (0...height).map { |y| (0...width).map { |x| [x, y] } }.reduce(&:+)
  end

  ## PART 2
  def unfold_map
    new_rows = (0...(5 * height)).map do |y|
      (0...(5 * width)).map { |x| unfolded_val(x, y) }
    end
    @rows = new_rows # need to keep original width/heigh values during unfolding
    @height = nil
    @width = nil
    puts to_s if ENV['DEBUG']
  end

  def to_s
    @rows.map(&:join).join("\n")
  end

  def unfolded_val(col, row)
    difh, fcol = col.divmod(width)
    difv, frow = row.divmod(height)
    return unless (temp = val(fcol, frow))

    temp += difv + difh
    temp -= 9 while temp > 9
    temp
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = Chiton.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
