# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './passage_pathing'

describe PassagePathing do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { PassagePathing.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  describe 'with small input' do
    let(:lines) do
      <<~TXT
        start-A
        start-b
        A-c
        A-b
        b-d
        A-end
        b-end
      TXT
    end

    it 'returns the basic answer' do
      _(subject.solution).must_equal(10)
    end

    it 'returns the advanced answer' do
      _(subject.solution).must_equal(36)
    end
  end

  describe 'with larger input' do
    let(:lines) do
      <<~TXT
        dc-end
        HN-start
        start-kj
        dc-start
        dc-HN
        LN-dc
        HN-end
        kj-sa
        kj-HN
        kj-dc
      TXT
    end

    it 'returns the correct answer' do
      _(subject.solution).must_equal(19)
    end

    it 'returns the correct advanced  answer' do
      _(subject.solution(advanced: true)).must_equal(103)
    end
  end

  describe 'with basic situation and even larger input' do
    let(:lines) do
      <<~TXT
        fs-end
        he-DX
        fs-he
        start-DX
        pj-DX
        end-zg
        zg-sl
        zg-pj
        pj-he
        RW-he
        fs-DX
        pj-RW
        zg-RW
        start-pj
        he-WI
        zg-he
        pj-fs
        start-RW
      TXT
    end

    it 'returns the correct answer' do
      _(subject.solution).must_equal(226)
    end

    it 'returns the correct advanced  answer' do
      _(subject.solution(advanced: true)).must_equal(3509)
    end
  end
end
