# frozen_string_literal: true

require 'debug'

# Advent of code - Day 12
class PassagePathing
  attr_reader :debug

  def initialize(filename, debug = false)
    data = File.readlines(filename).map(&:strip)
    @map = parse_data(data)
    @debug = debug
    log(@map.inspect)
  end

  def solution(advanced: false)
    paths(advanced:).size
  end

  def parse_data(lines)
    h = Hash.new { [] }
    pairs = lines.map { |l| l.split('-') }
    pairs.each_with_object(h) do |(a, b), map|
      map[b] += [a]
      map[a] += [b]
    end
  end

  def paths(advanced:)
    return [] unless @map.key?('start')

    recurse('start', advanced:).select { |x| x.last == 'end' }
  end

  def recurse(node, previous: [], advanced: false)
    log " - #{previous.join('-')}-#{node}..."
    return [node] if node == 'end'
    return [] unless @map.key? node

    candidates = send((advanced ? :visitable2 : :visitable), node, previous)

    candidates.each_with_object([[node]]) do |next_node, arr|
      recurse(next_node,
              previous: [*previous, node],
              advanced:).each do |a|
        arr << [node, *a] if [node, *a].last == 'end'
      end
    end
  end

  def visitable(node, previous)
    visited_small_caves = previous.select { |n| small_cave?(n) }
    @map[node] - [*visited_small_caves, 'start']
  end

  def visitable2(node, previous)
    visited = [*previous, node].select { |n| small_cave?(n) }.tally
    @map[node].each_with_object([]) do |c, keep|
      next if c == 'start'

      next unless c == c.upcase || # big caves any number
                  visited[c].nil? || # small caves first visit
                  first_small_revisited?(c, visited)

      keep << c
    end
  end

  def first_small_revisited?(node, visited)
    visited[node] == 1 && visited.values.none? { |v| v > 1 }
  end

  def small_cave?(node)= (node == node.downcase)

  def log(txt)
    puts(txt) if @debug
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = PassagePathing.new(ARGV[0], ENV['DEBUG'] == '1')
  puts obj.solution
  puts obj.solution(advanced: true)
end
