# frozen_string_literal: true

# Advent of code - Day 10
class SyntaxScoring
  attr_reader :lines

  def initialize(filename)
    @lines = File.readlines(filename).map(&:strip)
  end

  def solution(advanced: false)
    advanced ? completion_score : error_score
  end

  private

  def parsed_lines
    @parsed_lines ||= @lines.map { |l| parse_line(l) }
  end

  MISSING = { ')' => 3, ']' => 57, '}' => 1197, '>' => 25_137 }.freeze
  COMPLETE = { ')' => 1, ']' => 2, '}' => 3, '>' => 4 }.freeze
  PAIRS = { '(' => ')', '[' => ']', '{' => '}', '<' => '>' }.freeze

  def error_score
    error_lines.map { |l| l[:points] }.sum
  end

  def completion_score
    scores = incomplete_lines.map { |il| il[:points] }.sort
    scores[scores.length / 2]
  end

  def error_lines
    parsed_lines.select { |l| l[:status] == :error }
  end

  def incomplete_lines
    parsed_lines.select { |l| l[:status] == :incomplete }
  end

  def parse_line(line)
    rest = line.chars.each_with_object([]) do |c, stack|
      if opener?(c)
        stack << c
      else
        return error_line(c) unless PAIRS[stack.last] == c

        stack.pop
      end
    end
    incomplete_line(rest)
  end

  def error_line(char)
    { status: :error, points: MISSING[char] }
  end

  def incomplete_line(stack)
    { status: :incomplete, points: completion_points(stack) }
  end

  def completion_points(stack)
    completion(stack).inject(0) { |total, c| (total * 5) + COMPLETE[c] }
  end

  def completion(stack)
    stack.map { |c| PAIRS[c] }.reverse
  end

  def opener?(char)= PAIRS.keys.include?(char)
end

if __FILE__ == $PROGRAM_NAME
  obj = SyntaxScoring.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
