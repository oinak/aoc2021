# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './binary_diagnostic'

describe BinaryDiagnostic do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { BinaryDiagnostic.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      00100
      11110
      10110
      10111
      10101
      01111
      00111
      11100
      10000
      11001
      00010
      01010
    TXT
  end

  describe '#power_consumption' do
    it 'returns the correct poser consumption' do
      _(subject.power_consumption).must_equal(198)
    end
  end

  describe '#life_support calculation' do
    it 'returns the correct life o2 rating' do
      _(subject.send(:rating, :o2)).must_equal(23)
    end

    it 'returns the correct life co2 rating' do
      _(subject.send(:rating, :co2)).must_equal(10)
    end

    it 'returns the correct life support rating' do
      _(subject.life_support_rating).must_equal(230)
    end
  end
end
