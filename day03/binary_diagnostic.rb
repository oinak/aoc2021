# frozen_string_literal: true

# Advent of Code - Day 2
class BinaryDiagnostic
  attr_reader :values, :frequencies

  def initialize(filename)
    @values = read(filename)
    @frequencies = count_frequencies(@values)
  end

  def power_consumption
    gamma * epsilon
  end

  def life_support_rating
    rating(:o2) * rating(:co2)
  end

  private

  # Each position hols an histogram of symbol frequencies per that position
  # [{'0' => 12, '1'=> 3},{ '0'=> 3, '1' => 99}, ...]
  def count_frequencies(values)
    a = Array.new(values[0].size) { Hash.new(0) }
    values.each_with_object(a) do |value, list|
      bits = value.chars
      bits.each_with_index { |bit, index| list[index][bit] += 1 }
    end
  end

  def gamma
    most_common_bits(@frequencies).to_i(2)
  end

  def epsilon
    least_common_bits(@frequencies).to_i(2)
  end

  def most_common_bits(frequencies)
    frequencies.map { |h| h.max_by { |_, v| v }[0] }.join
  end

  def least_common_bits(frequencies)
    frequencies.map { |h| h.min_by { |_, v| v }[0] }.join
  end

  # get o2/co2 rating by selecting a single value from all, by bit_criteria
  def rating(criteria)
    (0..values[0].size).inject(values.dup) do |candidates, position|
      break candidates if candidates.size == 1 # no more selection possible

      filter_by_criteria(candidates, position, criteria)
    end.first.to_i(2)
  end

  # select values based on bit_criteria con one of the positions
  def filter_by_criteria(candidates, position, criteria)
    freqs = count_frequencies(candidates)
    candidates.select do |candidate|
      candidate[position] == bit_criteria(freqs, criteria)[position]
    end
  end

  # o2 wants most common and '1' when there is tie
  # c02 wants least common and '0' whene there is tie
  # max/min match both most/least and '0'<=>'1'
  def bit_criteria(frequencies, criteria)
    selector = criteria == :o2 ? :max : :min
    selected = frequencies.map do |h|
      h.select { |_, v| v == h.values.send(selector) } # not max_by because we can have ties
    end
    selected.map { |h| h.keys.send(selector) }
  end

  def read(filename)
    File.readlines(filename).map(&:strip)
  end
end

if __FILE__ == $PROGRAM_NAME
  bd = BinaryDiagnostic.new(ARGV[0])
  # puts bd.frequencies.inspect
  # puts bd.send(:most_common_bits).inspect
  puts bd.power_consumption
  puts bd.life_support_rating
end
