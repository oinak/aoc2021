# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './smoke_basin'

describe SmokeBasin do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { SmokeBasin.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      2199943210
      3987894921
      9856789892
      8767896789
      9899965678
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(15)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(1134)
    end
  end
end
