# frozen_string_literal: true

require 'debug'

# Advent of code - Day 9
class SmokeBasin
  def initialize(filename)
    data = File.readlines(filename)
    @map = data.map { |line| line.strip.chars.map(&:to_i) }
    @neighbors = {}
  end

  def solution(advanced: false)= advanced ? three_largest_basins : low_points_total_risk

  def to_s
    @map.map.with_index do |row, y|
      "#{format('%2d', y)} |#{row.map.with_index { |_, x| cell(x, y) }.join}"
    end.join("\n")
  end

  private

  ## PART 1

  def low_points_total_risk
    low_points.sum { |(x, y)| risk(x, y) }
  end

  def risk(posx, posy)= value(posx, posy) + 1

  def low_points
    @low_points ||= @map.each_with_object([]).with_index do |(row, list), y|
      row.each_with_index do |_value, x|
        list << [x, y] if low_point?(x, y)
      end
    end
  end

  def low_point?(posx, posy)
    neighbors(posx, posy).all? { |x, y| value(x, y) > value(posx, posy) }
  end

  # North West East South neighbors (if within boundaries)
  def neighbors(posx, posy)
    @neighbors[[posx, posy]] ||=
      begin
        nwes = [[posx, posy - 1], [posx - 1, posy], [posx + 1, posy], [posx, posy + 1]]
        nwes.select { |(x, y)| value(x, y) }
      end
  end

  def value(posx, posy)
    return nil if posx < 0 || posy < 0

    @map[posy]&.at(posx)
  end

  ## PART 2

  def three_largest_basins
    basins.map(&:size).sort.last(3).inject(:*)
  end

  def basins
    @basins ||= low_points.map { |lp| basin(lp) }
  end

  def basin_cells
    @basin_cells ||= basins.reduce(:+)
  end

  def basin(low)
    x, y = low
    list = basin_neighbors(x, y)
    basin_points = [[x, y]]
    until list.empty?
      p = list.pop
      candidates = basin_neighbors(*p)
      list |= (candidates - basin_points)
      basin_points << p
    end
    basin_points
  end

  def basin_neighbors(posx, posy)
    neighbors(posx, posy).select { |n| value(*n) != 9 && value(*n) > value(posx, posy) }
  end

  def cell(posx, posy)
    val = value(posx, posy)
    return low_color(val) if low_points.include?([posx, posy])
    return basin_color(val) if basin_cells.include?([posx, posy])

    val
  end

  def low_color(text)= "\e[0;31m#{text}\e[0m"

  def basin_color(text)= "\e[0;33m#{text}\e[0m"
end

if __FILE__ == $PROGRAM_NAME
  obj = SmokeBasin.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
  puts obj.to_s
end
