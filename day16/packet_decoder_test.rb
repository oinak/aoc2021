# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './packet_decoder'

describe PacketDecoder do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { PacketDecoder.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) { 'D2FE28' }

  describe 'with basic situation' do
    describe 'with small maessage' do
      let(:lines) { 'D2FE28' }
      it 'returns the correct answer' do
        _(subject.solution).must_equal(6)
      end
    end
    describe 'with medium message' do
      let(:lines) { '8A004A801A8002F478' }
      it 'returns the correct answer' do
        _(subject.solution).must_equal(16)
      end
    end
    describe 'with larger message' do
      let(:lines) { 'A0016C880162017C3686B18A3D4780' }
      it 'returns the correct answer' do
        _(subject.solution).must_equal(31)
      end
    end
  end

  describe 'with advanced situation' do
    describe 'with 1 + 2' do
      let(:lines) { 'C200B40A82' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(3)
      end
    end
    describe 'with 6 * 9' do
      let(:lines) { '04005AC33890' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(54)
      end
    end
    describe 'with min(7,8,9)' do
      let(:lines) { '880086C3E88112' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(7)
      end
    end
    describe 'with max(7,8,9)' do
      let(:lines) { 'CE00C43D881120' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(9)
      end
    end
    describe 'with less(5,15)' do
      let(:lines) { 'D8005AC2A8F0' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(1)
      end
    end
    describe 'with greater(5,15)' do
      let(:lines) { 'F600BC2D8F' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(0)
      end
    end
    describe 'with equal(5,15)' do
      let(:lines) { '9C005AC2F8F0' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(0)
      end
    end
    describe 'with equal(1 + 3, 2 * 2)' do
      let(:lines) { '9C0141080250320F1802104A08' }
      it 'returns the result of the operation' do
        _(subject.solution(advanced: true)).must_equal(1)
      end
    end
  end
end
