# frozen_string_literal: true

# Advent of code - Day 16
class PacketDecoder
  attr_accessor :bits, :counters

  def initialize(filename)
    data = File.read(filename)
    @bits = data.chars.map { |n| format('%04b', n.to_i(16)) }.join.chars
    @counters = { versions: 0, read: 0 }
  end

  def solution(advanced: false)
    advanced ? read_packet : sum_of_versions
  end

  def sum_of_versions
    read_packet
    counters[:versions]
  end

  TYPES = {
    0 => :sum,
    1 => :prod,
    2 => :min,
    3 => :max,
    4 => :literal,
    5 => :greater,
    6 => :less,
    7 => :equal
  }.freeze

  def read_bits(positions)
    counters[:read] += positions
    @bits.shift(positions).join
  end

  def read_packet(depth = 0)
    version, type = read_header
    counters[:versions] += version
    if TYPES[type] == :literal
      read_literal(depth + 2)
    else
      read_operator(type, depth + 2)
    end
  end

  def read_header
    version = read_bits(3).to_i(2)
    type = read_bits(3).to_i(2)
    [version, type].tap { |o| puts "Version: #{o[0]} type: #{o[1]}" if ENV['DEBUG'] }
  end

  def read_literal(depth = 0)
    numbers = []
    loop do
      packet = read_bits(5)
      numbers << packet[1..4]
      break if packet[0] == '0'
    end
    puts "#{' ' * depth}Literal #{numbers} = #{numbers.join.to_i(2)}" if ENV['DEBUG']
    numbers.join.to_i(2)
  end

  OPERATORS = {
    sum: ->(v) { v.sum },
    prod: ->(v) { v.reduce(:*) },
    min: ->(v) { v.min },
    max: ->(v) { v.max },
    # literal: ->(v) { v },
    greater: ->(v) { v[0] > v[1] ? 1 : 0 },
    less: ->(v) { v[0] < v[1] ? 1 : 0 },
    equal: ->(v) { v[0] == v[1] ? 1 : 0 }
  }.freeze

  def read_operator(type, depth = 0)
    values = read_operator_values(depth)
    OPERATORS[TYPES[type]].call(values).tap do |result|
      puts "#{' ' * depth}#{TYPES[type]}(#{values.inspect}) = #{result}" if ENV['DEBUG']
    end
  end

  def read_operator_values(depth)
    length_type_id = read_bits(1)
    if length_type_id == '0'
      length = read_bits(15).to_i(2)
      read_sp_length(length, depth)
    else # if length_type_id == '1' # '1'
      count = read_bits(11).to_i(2)
      read_sp_count(count, depth)
    end
  end

  def read_sp_count(count, depth)
    (0...count).map { read_packet(depth) }
  end

  def read_sp_length(length, depth)
    values = []
    start = counters[:read]
    values << read_packet(depth) until counters[:read] >= start + length
    values
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = PacketDecoder.new(ARGV[0])
  puts obj.solution
  obj = PacketDecoder.new(ARGV[0])
  puts obj.solution(advanced: true)
end
