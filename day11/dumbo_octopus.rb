# frozen_string_literal: true

require 'debug'

# Advent of code - Day 11
class DumboOctopus
  attr_reader :rows

  def initialize(filename)
    @rows = File.readlines(filename).map { |l| l.strip.chars.map(&:to_i) }
  end

  def to_s
    @rows.map { |row| row.map { |n| format('%3d', n) }.join }.join("\n")
  end

  def solution(advanced: false, steps: 100)
    advanced ? first_simultaneous : flashes_after(steps.to_i)
  end

  def flashes_after(steps)
    (0..steps).inject { |flashes, _| flashes + step }
  end

  def first_simultaneous
    current_step = 0
    until all? { |x, y| at(x, y) == 0 }
      step
      current_step += 1
    end
    current_step
  end

  private

  def step
    increase_energy
    flashed = []
    (flashable - flashed).each { |x, y| flashed |= [flash(x, y)] } until (flashable - flashed).empty?
    flashed.each { |x, y| set(x, y, 0) }.size
  end

  def increase_energy
    each { |x, y| inc(x, y) }
  end

  def flashable
    select { |x, y| at(x, y) > 9 }
  end

  def flash(posx, posy)
    neighbors(posx, posy).each { |x, y| inc(x, y) if at(x, y) }
    [posx, posy]
  end

  def at(posx, posy)= posx >= 0 && posy >= 0 ? @rows[posy]&.[](posx) : nil

  def set(posx, posy, value)= @rows[posy][posx] = value

  def inc(posx, posy)= @rows[posy][posx] += 1

  include Enumerable

  def each
    @rows.each_with_index { |row, y| row.each_index { |x| yield(x, y) } }
  end

  DIFFS = [-1, 0, 1].freeze
  NEIGH = DIFFS.flat_map { |x| DIFFS.map { |y| [x, y] } } - [[0, 0]]
  def neighbors(posx, posy)
    NEIGH.map { |dx, dy| [posx + dx, posy + dy] }.select { |x, y| at(x, y) }
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = DumboOctopus.new(ARGV[0])
  puts obj.solution(steps: ARGV[1])
  obj2 = DumboOctopus.new(ARGV[0])
  puts obj2.solution(advanced: true)
end
