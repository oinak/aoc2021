# frozen_string_literal: true

require 'debug'

# Advent of Coode - Day 3
module GiantSquid
  # Game class
  class Bingo
    attr_reader :data, :boards, :debug

    def initialize(filename, debug: false)
      @debug = debug
      read(filename)
    end

    def read(filename)
      @data = File.read(filename)
      parts = data.split("\n\n")
      numbers_data, *boards_data = parts
      @numbers = numbers_data.split(',')
      @boards = boards_data.map { |board_data| Board.new(board_data) }
    end

    def play
      @numbers.each do |number|
        @boards.each { |board| board.mark(number) }
        winner = @boards.find(&:winner?)
        display(number)
        break winner.score if winner
      end
    end

    def play_to_loose
      won = []
      playing = @boards
      @numbers.each do |number|
        puts "won: #{won.size} playing: #{playing.size}" if debug
        won, playing = update_board_lists(won, playing, number)
        display(number)
        break won.last.score if playing.size.zero?
      end
    end

    def update_board_lists(won, playing, number)
      # won << playing.delete(board) if board.winner? # did not work, each uses index
      playing.map { |board| board.mark(number) }
      won |= playing.select(&:winner?)
      playing.reject!(&:winner?)
      [won, playing]
    end

    def display(number)
      return unless debug

      puts "\nBall extracted: (#{number})\n#{@boards.map(&:to_s).join("\n")}"
    end
  end

  # Card class
  class Board
    attr_reader :rows, :cols, :marked, :i

    def initialize(board_data)
      @rows = board_data.split("\n").map { |line| line.split.map(&:to_i) }
      @cols = @rows.transpose
      @marked = []
    end

    def to_s = "#{rows.map { |r| row_to_s(r) }.join("\n")}\n#{winner? ? "Winner!\n" : ''}"

    def mark(number) = @marked << number.to_i

    def winner? = lines.any? { |line| complete?(line) }

    def score = unmarked.sum * @marked.last

    private

    def row_to_s(row) = "|#{row.map { |c| cell_to_s(c) }.join}|"

    def cell_to_s(number) = (@marked.include?(number) ? ' XX' : '%3d') % number

    def lines = rows + cols

    def complete?(line) = line.all? { |cell| marked.include? cell }

    def unmarked = cols.flatten - marked
  end
end

if __FILE__ == $PROGRAM_NAME
  bingo = ::GiantSquid::Bingo.new(ARGV[0], debug: true)
  if ENV['GOAL'] == 'win'
    puts bingo.play
  else
    puts bingo.play_to_loose
  end
end
