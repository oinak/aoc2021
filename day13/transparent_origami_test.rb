# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './transparent_origami'

describe TransparentOrigami do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { TransparentOrigami.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      6,10
      0,14
      9,10
      0,3
      10,4
      4,11
      6,0
      6,12
      4,1
      0,13
      10,12
      3,4
      3,0
      8,4
      1,10
      2,14
      8,10
      9,0

      fold along y=7
      fold along x=5
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(17)
    end
    describe 'with vertical fold' do
      let(:lines) do
        <<~TXT
          6,10
          0,14
          9,10
          0,3
          10,4
          4,11
          6,0
          6,12
          4,1
          0,13
          10,12
          3,4
          3,0
          8,4
          1,10
          2,14
          8,10
          9,0

          fold along x=5
        TXT
      end
      it 'returns the correct answer' do
        _(subject.solution).must_equal(17)
      end
    end
  end

  describe 'after completaing all the folds' do
    let(:symbol) { "#####\n#...#\n#...#\n#...#\n#####\n.....\n....." }
    it 'outputs the code in # sings' do
      solution = subject.solution(advanced: true)
      _(solution).must_equal(symbol)
    end
  end
end
