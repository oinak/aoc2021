# frozen_string_literal: true

require 'debug'

# Advent of code - Day 13
class TransparentOrigami
  def initialize(filename)
    data = File.read(filename)
    parse(data)
    display
    log @folds.inspect
  end

  def solution(advanced: false)
    advanced ? complete_folds : dots_after_first_fold
  end

  def dots_after_first_fold
    axis, value = @folds[0]
    axis == 'x' ? fold_x(value) : fold_y(value)

    (0..@height).sum { |y| (0..@width).count { |x| get(x, y) } }
  end

  def complete_folds
    @folds.each do |axis, value|
      axis == 'x' ? fold_x(value) : fold_y(value)
    end
    to_s
  end

  private

  DOT = true
  BLANK = false

  def parse(data)
    dots_data, folds_data = data.split("\n\n")
    @dots = parse_dots(dots_data)
    @folds = parse_folds(folds_data)
  end

  def parse_dots(data)
    coordinates = data.split("\n").map { |line| line.split(',').map(&:to_i) }
    @width, @height = coordinates.transpose.map(&:max)
    @values = coordinates.each_with_object(Hash.new(false)) do |(x, y), values|
      values[[x, y]] = true
    end
  end

  def parse_folds(folds_data)
    @folds = folds_data.split("\n").map do |line|
      axis, value = line.split('=')
      [axis[-1], value.to_i]
    end
  end

  def to_s
    lines = (0..@height).map { |y| (0..@width).map { |x| dot(get(x, y)) }.join }
    lines.join("\n")
  end

  def display
    log(to_s)
  end

  def dot(value)
    value ? '#' : '.'
  end

  def log(msg)
    return unless ENV['DEBUG']

    puts msg
  end

  def fold_y(posy)
    (0..posy).each { |y| combine_line(y) }
    @height = posy - 1
  end

  def fold_x(posx)
    (0..posx).each { |x| combine_col(x) }
    @width = posx - 1
  end

  def combine_line(posy)
    (0..@width).each do |x|
      set(x, posy, get(x, posy) || get(x, @height - posy))
    end
  end

  def combine_col(posx)
    (0..@height).each do |y|
      set(posx, y, get(posx, y) || get(@width - posx, y))
    end
  end

  def get(posx, posy)
    @values[[posx, posy]]
  end

  def set(posx, posy, val)
    @values[[posx, posy]] = val
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = TransparentOrigami.new(ARGV[0])
  puts obj.solution
  obj2 = TransparentOrigami.new(ARGV[0])
  puts obj2.solution(advanced: true)
end
