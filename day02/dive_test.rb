# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './dive'

describe Dive do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { Dive.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      forward 5
      down 5
      forward 8
      up 3
      down 8
      forward 2
    TXT
  end

  describe '#dive' do
    it 'returns the correct horizontal x depth' do
      _(subject.dive).must_equal(150)
    end
  end

  describe '#dive with aim' do
    it 'returns the correct position using aim' do
      _(subject.dive(with_aim: true)).must_equal(900)
    end
  end
end
