# frozen_string_literal: true

# Advent of Code - Day 2
class Dive
  attr_reader :values

  DEFAULT_STATE = { horizontal: 0, depth: 0, aim: 0 }.freeze

  def initialize(filename)
    @values = read(filename)
    reset
  end

  def reset
    @position = DEFAULT_STATE.dup
  end

  def dive(with_aim: false)
    result(values, with_aim:)
  end

  private

  def read(filename)
    File.readlines(filename).map(&:strip)
  end

  def result(values, with_aim:)
    values.each do |input|
      action, distance = input.split
      update(action, distance.to_i, with_aim:)
    end
    @position[:horizontal] * @position[:depth]
  end

  def update(action, distance, with_aim:)
    with_aim ? update_aim(action, distance.to_i) : update_basic(action, distance.to_i)
  end

  def update_basic(action, distance)
    case action
    when 'forward' then @position[:horizontal] += distance
    when 'up' then @position[:depth] -= distance
    when 'down' then @position[:depth] += distance
    end
  end

  def update_aim(action, distance)
    # s = "#{ @position.inspect} a:#{action} d:#{distance} >>>"
    case action
    when 'forward'
      @position[:horizontal] += distance
      @position[:depth] += distance * @position[:aim] if @position[:aim] != 0
    when 'up'
      @position[:aim] -= distance
    when 'down'
      @position[:aim] += distance
    end
    # puts "#{s} #{ @position.inspect}\n\n"
  end
end

if __FILE__ == $PROGRAM_NAME
  dive = Dive.new(ARGV[0])
  puts dive.dive
  dive.reset
  puts dive.dive(with_aim: true)
end
