# frozen_string_literal: true

# require 'debug'

# Advent of code - Day 14
class ExtendedPolymerization
  def initialize(filename)
    data = File.read(filename)
    template, insertions = data.split("\n\n")
    @chain = template.chars
    @rules = parse_insertions(insertions.split("\n"))
  end

  def solution(advanced: false)
    advanced ? popularity_advanced(40) : popularity_amplitude(10)
  end

  private

  ## PART 1

  def popularity_amplitude(steps)
    chain = @chain
    steps.times { chain = insert(chain) }
    min, max = chain.tally.minmax_by { |_, v| v }
    max[1] - min[1]
  end

  def parse_insertions(data)
    data.each_with_object({}) do |line, h|
      pair, insert = line.split(' -> ')
      h[pair] = insert
    end
  end

  def insert(chain)
    chain.inject([]) do |list, item|
      if list[0]
        left = list[-1]
        pair = [left, item].join
        list + [@rules[pair], item]
      else
        list + [item]
      end
    end
  end

  ## PART 2

  def popularity_advanced(steps)
    pairs = Hash.new(0).merge(@chain.each_cons(2).map(&:join).tally)
    letters = Hash.new(0).merge(@chain.tally)
    steps.times { pairs, letters = insert_pairs(pairs, letters) }

    min, max = letters.minmax_by { |_, v| v }
    max[1] - min[1]
  end

  def insert_pairs(pairs, letters)
    new_pairs = pairs.each_with_object(pairs.dup) do |(pair_key, pair_count), accumulator|
      update_counts(accumulator, letters, pair_key, pair_count) if pair_count > 0
    end
    [new_pairs, letters]
  end

  def update_counts(accumulator, letters, pair_key, pair_count)
    accumulator[pair_key] -= pair_count # remove source pairs
    infix_letter = @rules[pair_key]

    left = [pair_key[0], infix_letter].join
    right = [infix_letter, pair_key[1]].join

    accumulator[left] += pair_count     # add target pairs (lefts)
    accumulator[right] += pair_count    # add target pairs (rights)
    letters[infix_letter] += pair_count # update letter tally
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = ExtendedPolymerization.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
