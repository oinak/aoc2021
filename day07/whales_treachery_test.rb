# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './whales_treachery'

describe WhalesTreachery do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { WhalesTreachery.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      16,1,2,0,4,2,7,1,2,14
    TXT
  end

  describe 'with linear fuel' do
    it 'returns the cost of cheapest aligment' do
      _(subject.cheapest_aligment).must_equal(37)
    end
  end

  describe 'with growing cost of fuel' do
    it 'returns the cost of cheapest aligment' do
      _(subject.cheapest_aligment(cost: WhalesTreachery::Growing)).must_equal(168)
    end
  end
end
