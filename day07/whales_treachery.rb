# frozen_string_literal: true

class WhalesTreachery
  attr_reader :positions

  def initialize(filename)
    data = File.read(filename)
    @positions = data.strip.split(',').map(&:to_i)
  end

  def cheapest_aligment(cost: Linear)
    min, max = positions.minmax
    # (min..max).inject({}){ |h,n| h.merge(n => positions.map{ |p| (p-n).abs }.sum) }.min_by{|k,v| v}.last
    (min..max).inject({}) do |h, n|
      costs = positions.map { |p| cost.value(p - n) }
      h.merge(n => costs.sum)
    end.min_by { |_k, v| v }.last
  end

  module Linear
    extend self

    def value(diff)
      diff.abs
    end
  end

  module Growing
    extend self

    def value(diff)
      # cache[diff] ||= (1..(diff.abs)).reduce(:+) || 0
      cache[diff] ||= diff * (diff + 1) / 2 # sponsored by @freaktyful knowing math
    end

    def cache
      @cache ||= {}
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  wt = WhalesTreachery.new(ARGV[0])
  puts wt.cheapest_aligment
  puts wt.cheapest_aligment(cost: ::WhalesTreachery::Growing)
end
