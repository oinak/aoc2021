# frozen_string_literal: true

# Advent of code - Day 8
class SevenSegment
  UNIQUE_SEGMENT_COUNT = { 2 => 1, 3 => 7, 4 => 4, 7 => 8 }.freeze

  DIGITS = {
    'cf' => '1', 'acdeg' => '2', 'acdfg' => '3',
    'bcdf' => '4', 'abdfg' => '5', 'abdefg' => '6',
    'acf' => '7', 'abcdefg' => '8', 'abcdfg' => '9',
    'abcefg' => '0'
  }.freeze

  def initialize(filename)
    @debug = ENV['DEBUG'] == '1'
    data = File.readlines(filename)
    @lines = data.inject([]) do |list, str|
      input, output = str.split(' | ')
      list << { input: input.split, output: output.split }
    end
  end

  def solution(advanced: false)
    advanced ? sum_outputs : unique_digits_in_output
  end

  ## PART 1

  def unique_digits_in_output
    @lines.inject(0) { |sum, line| sum + line[:output].count { |o| unique?(o) } }
  end

  def unique?(segments)= UNIQUE_SEGMENT_COUNT.keys.include?(segments.size)

  ## PART 2

  def sum_outputs
    @lines.inject(0) { |sum, line| sum + line_value(line) }
  end

  def line_value(line)
    log "\nLINE: #{line[:input].join(' ')} | #{line[:output].join(' ')}"
    translations = line_translations(line)
    log "- translations: #{translations.inspect}"

    line[:output].flat_map do |word|
      segment_group = word_to_segments(word, translations)
      segments_to_numbers(segment_group)
    end.join.to_i
  end

  #                               | appearances:
  #        a: 0   2 3   5 6 7 8 9 | 8 (2)
  #        b: 0       4 5 6   8 9 | 6 *
  #        c: 0 1 2 3 4     7 8 9 | 8 (2)
  #        d:     2 3 4 5 6   8 9 | 7 (2)
  #        e: 0   2       6   8   | 4 *
  #        f: 0 1   3 4 5 6 7 8 9 | 9 *
  #        g: 0   2 3   5 6   8 9 | 7 (2)
  #        -----------------------+
  # segments: 6 2 5 5 4 5 6 3 8 6 |
  #           - * - - * - - * * -
  def line_translations(line)
    segments = line[:input].each_with_object({}) do |digit, known|
      number = UNIQUE_SEGMENT_COUNT[digit.size]
      known[number] ||= digit.chars if number
    end

    appearances = line[:input].join.chars.tally

    log "--segments: #{segments.inspect}"
    log "--appearances: #{appearances.inspect}"
    translations_from_line_data(appearances, segments)
  end

  def translations_from_line_data(appearances, segments) # rubocop:disable Metrics/AbcSize
    {
      'a' => (segments[7] - segments[1])[0],
      'b' => (b = appearances.invert[6]),
      'e' => (e = appearances.invert[4]),
      'f' => (f = appearances.invert[9]),
      'c' => (c = (segments[1] - [f])[0]),
      'd' => (d = (segments[4] - [b, c, f])[0]),
      'g' => (segments[8] - segments[7] - [b, d, e])[0]
    }.tap { |t| display_digit(t) }.invert
  end

  # sorted to be trivial to find in DIGITS
  def word_to_segments(word, translations)= word.chars.map { |c| translations[c] }.sort.join

  def segments_to_numbers(segment_group)= DIGITS[segment_group.chars.sort.join]

  def display_digit(translations)
    t = translations
    log " ------\n #{t['a'] * 4}\n" \
        "#{t['b']}    #{t['c']}\n#{t['b']}    #{t['c']}\n #{t['d'] * 4}\n" \
        "#{t['e']}    #{t['f']}\n#{t['e']}    #{t['f']}\n #{t['g'] * 4}\n"
  end

  def log(msg)
    puts msg if @debug
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = SevenSegment.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
