# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './seven_segment'

describe SevenSegment do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { SevenSegment.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
      edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
      fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
      fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
      aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
      fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
      dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
      bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
      egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
      gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(26)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(61_229)
    end

    describe 'calculations' do
      let(:line) do
        {
          input: %w[acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab],
          output: %w[cdfeb fcadb cdfeb cdbaf]
        }
      end
      let(:translations) do
        {
          'd' => 'a', 'e' => 'b', 'a' => 'c', 'f' => 'd', 'g' => 'e', 'b' => 'f', 'c' => 'g'
        }
      end

      it 'returns the correct line translations' do
        _(subject.line_translations(line)).must_equal(translations)
      end

      it 'returns the correct line translations' do
        _(subject.word_to_segments('ab', translations)).must_equal('cf')
        _(subject.word_to_segments('dab', translations)).must_equal('acf')
        _(subject.word_to_segments('cdfeb', translations)).must_equal('abdfg')
      end

      it 'returns the correct number from translated segments' do
        _(subject.segments_to_numbers('cf')).must_equal('1')
        _(subject.segments_to_numbers('acf')).must_equal('7')
        _(subject.segments_to_numbers('abdfg')).must_equal('5')
      end
    end
  end
end
