# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './sonar'

describe Sonar do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { Sonar.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    %w[199 200 208 210 200 207 240 269 260 263].join("\n")
  end

  describe '#run' do
    it 'returns the correct number of increasing values' do
      _(subject.sweep).must_equal(7)
    end
  end

  describe '#sweep with triplets' do
    it 'returns the correct number of increasing suyms of triplets' do
      _(subject.sliding_sweep).must_equal(5)
    end
  end
end
