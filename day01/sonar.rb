# frozen_string_literal: true

# Advent of Code - Day 1
class Sonar
  attr_reader :values

  def initialize(filename)
    @values = read(filename)
  end

  def sweep
    result(values)
  end

  def sliding_sweep
    sums = values.each_cons(3).map(&:sum)
    result(sums)
  end

  private

  def read(filename)
    File.readlines(filename).map { |l| l.strip.to_i }
  end

  def result(values)
    values.each_cons(2).count { |a, b| b > a }
  end
end

if __FILE__ == $PROGRAM_NAME
  sonar = Sonar.new(ARGV[0])
  puts sonar.sweep
  puts sonar.sliding_sweep
end
