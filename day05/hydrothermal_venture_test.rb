# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './hydrothermal_venture'

describe HydrothermalVenture do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { HydrothermalVenture.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      0,9 -> 5,9
      8,0 -> 0,8
      9,4 -> 3,4
      2,2 -> 2,1
      7,0 -> 7,4
      6,4 -> 2,0
      0,9 -> 2,9
      3,4 -> 1,4
      0,0 -> 8,8
      5,5 -> 8,2
    TXT
  end

  let(:diagram) do
    <<~TXT
      .......1..
      ..1....1..
      ..1....1..
      .......1..
      .112111211
      ..........
      ..........
      ..........
      ..........
      222111....
    TXT
  end

  # it 'produces the expected points list' do
  #   _(subject.points([1, 1], [1, 3])).must_equal([[1, 1], [1, 2], [1, 3]])
  #   _(subject.points([9, 7], [7, 7])).must_equal([[9, 7], [8, 7], [7, 7]])
  # end

  it 'produces the expected diagram' do
    _(subject.diagram).must_equal(diagram.strip)
  end

  it 'counts the number of overapping points' do
    _(subject.overlaps).must_equal(5)
  end

  describe 'with diagonals' do
    subject { HydrothermalVenture.new(@tempfile.path, diagonals: true) }
    let(:diagram) do
      <<~TXT
        1.1....11.
        .111...2..
        ..2.1.111.
        ...1.2.2..
        .112313211
        ...1.2....
        ..1...1...
        .1.....1..
        1.......1.
        222111....
      TXT
    end

    it 'produces the expected diagram' do
      _(subject.diagram).must_equal(diagram.strip)
    end

    it 'counts the number of overapping points' do
      _(subject.overlaps).must_equal(12)
    end
  end
end
