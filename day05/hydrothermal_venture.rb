# frozen_string_literal: true

# Advent of Code - Day 5
class HydrothermalVenture
  attr_reader :points

  def initialize(filename, diagonals: false)
    @diagonals = diagonals
    text_lines = File.readlines(filename)
    setup(text_lines)
  end

  # count number of points with two or more lines crossing
  def overlaps = @densities.values.count { |v| v >= 2 }

  # display density map as is the stament
  def diagram = 0.upto(width).map { |y| 0.upto(width).map { |x| symbol(x, y) }.join }.join("\n")

  private

  def setup(text_lines)
    @densities = text_lines.flat_map do |line|
      x1, y1, x2, y2 = endpoints_from_text(line)
      lines_from_endpoints(x1, y1, x2, y2) # .tap { |r| puts "#{xy1}->#{xy2} #{r}"}
    end.tally
  end

  ## DIAGRAM aux code

  # assumes with is power of 10, 0-9, 0-99, 0-999...
  def width = @width ||= (10**(Math.log10(@densities.keys.flatten.max).to_i + 1)) - 1

  def symbol(posx, posy) = @densities[[posx, posy]] || '.'

  ## SETUP aux code

  # "2,3 -> 2,1" => [2, 3, 2, 1]
  def endpoints_from_text(text) = text.split(' -> ').map { |p| p.split(',').map(&:to_i) }.flatten

  # [2, 3, 2, 1] => [[2,3],[2,2],[2,1]]
  def lines_from_endpoints(px1, py1, px2, py2)
    return vertical(px1, py1, py2) if px1 == px2
    return horizontal(px1, px2, py1) if py1 == py2

    @diagonals ? diagonal(px1, py1, px2, py2) : []
  end

  def vertical(px1, py1, py2) = vector(px1, py1, py2)

  # generate y,x pairs, reverse pre return
  def horizontal(px1, px2, py1) = vector(py1, px1, px2).map(&:reverse)

  def vector(fix, p_a, p_b) = [p_a, p_b].min.upto([p_a, p_b].max).map { |var| [fix, var] }

  # 0,1 -> 3,4 = [0,1][1,2][3,4]
  # 1,5 -> 4,2 = [1,5][2,4][3,3][4,2]
  def diagonal(px1, py1, px2, py2)
    pxx = (px1 < px2 ? px1.upto(px2) : px1.downto(px2))
    pyy = (py1 < py2 ? py1.upto(py2) : py1.downto(py2))
    pxx.zip(pyy)
  end
end

if __FILE__ == $PROGRAM_NAME
  puts HydrothermalVenture.new(ARGV[0]).overlaps
  puts HydrothermalVenture.new(ARGV[0], diagonals: true).overlaps
end
