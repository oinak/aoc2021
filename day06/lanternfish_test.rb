# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './lanternfish.rb'

describe Lanternfish do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { Lanternfish }

  after do
    @tempfile.delete
  end

  let(:lines) { '3,4,3,1,2' }

  it 'calculates the right number of fish after 80 days' do
    _(subject.run(@tempfile.path, 80)).must_equal(5934)
  end

  it 'calculates the right number of fish after 256 days' do
    _(subject.run(@tempfile.path, 256)).must_equal(26_984_457_539)
  end

  describe 'with alternate implementation' do
    it 'calculates the right number of fish after 80 days' do
      _(subject.run_alt(@tempfile.path, 80)).must_equal(5934)
    end

    it 'calculates the right number of fish after 256 days' do
      _(subject.run_alt(@tempfile.path, 256)).must_equal(26_984_457_539)
    end
  end
end
