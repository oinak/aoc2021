# frozen_string_literal: true

# Advent of code - Day 6
module Lanternfish
  extend self

  def run(filename, steps)
    school = setup(filename)

    steps.to_i.times { step(school) }

    result(school)
  end

  def run_alt(filename, steps)
    setup(filename).tap do |h|
      steps.to_i.times do
        h.transform_keys! { |k| k - 1 }.merge!(6 => h[6] + h[-1], 8 => h[-1]).delete(-1)
      end
    end.values.sum
  end

  private

  def setup(filename)
    data = File.read(filename)
    school = Hash.new(0)
    school.merge!(data.split(',').map(&:to_i).tally)
  end

  def step(school)
    spawns = school.delete(0) || 0
    school.transform_keys! { |k| k - 1 }
    school[8] = spawns
    school[6] += spawns
  end

  def result(school)= school.values.sum
end

if __FILE__ == $PROGRAM_NAME
  puts Lanternfish.run(ARGV[0], ARGV[1])
  puts Lanternfish.run_alt(ARGV[0], ARGV[1])
end
